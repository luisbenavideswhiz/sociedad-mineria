import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lineamientos',
  templateUrl: './lineamientos.component.html',
  styleUrls: ['./lineamientos.component.css']
})
export class LineamientosComponent implements OnInit {

  verpoliticas: boolean;

  constructor() {

    this.verpoliticas = false;

   }

  ngOnInit(): void {
  }

  funcionverpoliticas() {
    this.verpoliticas = !this.verpoliticas;
  }

}
