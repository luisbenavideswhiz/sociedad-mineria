import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectosenexploracionComponent } from './proyectosenexploracion.component';

describe('ProyectosenexploracionComponent', () => {
  let component: ProyectosenexploracionComponent;
  let fixture: ComponentFixture<ProyectosenexploracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectosenexploracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectosenexploracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
