import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvancedeproyectosComponent } from './avancedeproyectos.component';

describe('AvancedeproyectosComponent', () => {
  let component: AvancedeproyectosComponent;
  let fixture: ComponentFixture<AvancedeproyectosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvancedeproyectosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvancedeproyectosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
