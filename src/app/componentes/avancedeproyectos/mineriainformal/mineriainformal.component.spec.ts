import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MineriainformalComponent } from './mineriainformal.component';

describe('MineriainformalComponent', () => {
  let component: MineriainformalComponent;
  let fixture: ComponentFixture<MineriainformalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MineriainformalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MineriainformalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
