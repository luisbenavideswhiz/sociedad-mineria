import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectosenconstruccionComponent } from './proyectosenconstruccion.component';

describe('ProyectosenconstruccionComponent', () => {
  let component: ProyectosenconstruccionComponent;
  let fixture: ComponentFixture<ProyectosenconstruccionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectosenconstruccionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectosenconstruccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
