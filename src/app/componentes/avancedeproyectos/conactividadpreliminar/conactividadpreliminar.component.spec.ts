import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConactividadpreliminarComponent } from './conactividadpreliminar.component';

describe('ConactividadpreliminarComponent', () => {
  let component: ConactividadpreliminarComponent;
  let fixture: ComponentFixture<ConactividadpreliminarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConactividadpreliminarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConactividadpreliminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
