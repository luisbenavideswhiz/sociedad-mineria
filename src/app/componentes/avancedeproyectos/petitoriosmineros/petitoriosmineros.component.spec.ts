import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PetitoriosminerosComponent } from './petitoriosmineros.component';

describe('PetitoriosminerosComponent', () => {
  let component: PetitoriosminerosComponent;
  let fixture: ComponentFixture<PetitoriosminerosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PetitoriosminerosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetitoriosminerosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
