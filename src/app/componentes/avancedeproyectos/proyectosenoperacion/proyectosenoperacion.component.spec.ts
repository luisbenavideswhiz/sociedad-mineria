import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectosenoperacionComponent } from './proyectosenoperacion.component';

describe('ProyectosenoperacionComponent', () => {
  let component: ProyectosenoperacionComponent;
  let fixture: ComponentFixture<ProyectosenoperacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProyectosenoperacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectosenoperacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
