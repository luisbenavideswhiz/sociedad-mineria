import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicecontenidosComponent } from './indicecontenidos.component';

describe('IndicecontenidosComponent', () => {
  let component: IndicecontenidosComponent;
  let fixture: ComponentFixture<IndicecontenidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndicecontenidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicecontenidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
