import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IniciosostenibilidadambientalComponent } from './iniciosostenibilidadambiental.component';

describe('IniciosostenibilidadambientalComponent', () => {
  let component: IniciosostenibilidadambientalComponent;
  let fixture: ComponentFixture<IniciosostenibilidadambientalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IniciosostenibilidadambientalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IniciosostenibilidadambientalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
