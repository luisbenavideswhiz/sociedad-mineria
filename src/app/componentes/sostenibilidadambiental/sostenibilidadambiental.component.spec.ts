import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SostenibilidadambientalComponent } from './sostenibilidadambiental.component';

describe('SostenibilidadambientalComponent', () => {
  let component: SostenibilidadambientalComponent;
  let fixture: ComponentFixture<SostenibilidadambientalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SostenibilidadambientalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SostenibilidadambientalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
