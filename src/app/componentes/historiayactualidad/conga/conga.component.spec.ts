import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CongaComponent } from './conga.component';

describe('CongaComponent', () => {
  let component: CongaComponent;
  let fixture: ComponentFixture<CongaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CongaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CongaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
