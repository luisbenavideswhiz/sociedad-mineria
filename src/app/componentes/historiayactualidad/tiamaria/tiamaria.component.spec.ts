import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiamariaComponent } from './tiamaria.component';

describe('TiamariaComponent', () => {
  let component: TiamariaComponent;
  let fixture: ComponentFixture<TiamariaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiamariaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiamariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
