import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TambograndeComponent } from './tambogrande.component';

describe('TambograndeComponent', () => {
  let component: TambograndeComponent;
  let fixture: ComponentFixture<TambograndeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambograndeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TambograndeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
