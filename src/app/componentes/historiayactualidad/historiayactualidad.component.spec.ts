import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoriayactualidadComponent } from './historiayactualidad.component';

describe('HistoriayactualidadComponent', () => {
  let component: HistoriayactualidadComponent;
  let fixture: ComponentFixture<HistoriayactualidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoriayactualidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoriayactualidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
