import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RioblancoComponent } from './rioblanco.component';

describe('RioblancoComponent', () => {
  let component: RioblancoComponent;
  let fixture: ComponentFixture<RioblancoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RioblancoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RioblancoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
