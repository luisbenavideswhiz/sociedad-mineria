import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EfectoeconomiaComponent } from './efectoeconomia.component';

describe('EfectoeconomiaComponent', () => {
  let component: EfectoeconomiaComponent;
  let fixture: ComponentFixture<EfectoeconomiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EfectoeconomiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EfectoeconomiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
