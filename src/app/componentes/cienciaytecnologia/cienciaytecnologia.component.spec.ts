import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CienciaytecnologiaComponent } from './cienciaytecnologia.component';

describe('CienciaytecnologiaComponent', () => {
  let component: CienciaytecnologiaComponent;
  let fixture: ComponentFixture<CienciaytecnologiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CienciaytecnologiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CienciaytecnologiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
