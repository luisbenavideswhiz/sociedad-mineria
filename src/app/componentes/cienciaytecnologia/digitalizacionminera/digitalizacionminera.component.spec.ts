import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DigitalizacionmineraComponent } from './digitalizacionminera.component';

describe('DigitalizacionmineraComponent', () => {
  let component: DigitalizacionmineraComponent;
  let fixture: ComponentFixture<DigitalizacionmineraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DigitalizacionmineraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DigitalizacionmineraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
