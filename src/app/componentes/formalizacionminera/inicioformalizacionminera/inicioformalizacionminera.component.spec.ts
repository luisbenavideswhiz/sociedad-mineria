import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioformalizacionmineraComponent } from './inicioformalizacionminera.component';

describe('InicioformalizacionmineraComponent', () => {
  let component: InicioformalizacionmineraComponent;
  let fixture: ComponentFixture<InicioformalizacionmineraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InicioformalizacionmineraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioformalizacionmineraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
