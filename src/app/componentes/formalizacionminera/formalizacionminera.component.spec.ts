import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormalizacionmineraComponent } from './formalizacionminera.component';

describe('FormalizacionmineraComponent', () => {
  let component: FormalizacionmineraComponent;
  let fixture: ComponentFixture<FormalizacionmineraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormalizacionmineraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormalizacionmineraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
