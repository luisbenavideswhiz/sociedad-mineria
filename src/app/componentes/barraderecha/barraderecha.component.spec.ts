import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarraderechaComponent } from './barraderecha.component';

describe('BarraderechaComponent', () => {
  let component: BarraderechaComponent;
  let fixture: ComponentFixture<BarraderechaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarraderechaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarraderechaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
