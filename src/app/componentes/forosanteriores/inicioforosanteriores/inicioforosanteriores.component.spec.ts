import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioforosanterioresComponent } from './inicioforosanteriores.component';

describe('InicioforosanterioresComponent', () => {
  let component: InicioforosanterioresComponent;
  let fixture: ComponentFixture<InicioforosanterioresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InicioforosanterioresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioforosanterioresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
