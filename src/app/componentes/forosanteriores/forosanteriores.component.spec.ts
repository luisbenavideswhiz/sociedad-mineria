import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForosanterioresComponent } from './forosanteriores.component';

describe('ForosanterioresComponent', () => {
  let component: ForosanterioresComponent;
  let fixture: ComponentFixture<ForosanterioresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForosanterioresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForosanterioresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
