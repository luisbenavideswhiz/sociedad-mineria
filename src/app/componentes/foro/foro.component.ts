import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-foro',
  templateUrl: './foro.component.html',
  styleUrls: ['./foro.component.css']
})
export class ForoComponent implements OnInit {

  ingresar: string[];
  vercomentarios: boolean;
  vertemas: boolean;

  constructor() { 
    this.ingresar = ["Es bueno que haya espacios de este tipo. Estaré atento a sus publicaciones, porque en mi región hay muchas dudas sobre cómo están funcionando las empresas mineras."];
    this.vercomentarios = false;
    this.vertemas = false;
  }

  ngOnInit(): void {
  }

  nuevoIngreso(ingreso) {
    this.ingresar.push(ingreso.value);
    ingreso.value = '';
    return false;
  }

  funcionvercomentarios() {
    this.vercomentarios = !this.vercomentarios;
  }

  funcionvertemas(){
    this.vertemas = !this.vertemas;
  }


 

}
