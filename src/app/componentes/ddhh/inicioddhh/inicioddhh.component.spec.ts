import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioddhhComponent } from './inicioddhh.component';

describe('InicioddhhComponent', () => {
  let component: InicioddhhComponent;
  let fixture: ComponentFixture<InicioddhhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InicioddhhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioddhhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
