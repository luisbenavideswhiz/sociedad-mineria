import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IniciodesarrolloregionalComponent } from './iniciodesarrolloregional.component';

describe('IniciodesarrolloregionalComponent', () => {
  let component: IniciodesarrolloregionalComponent;
  let fixture: ComponentFixture<IniciodesarrolloregionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IniciodesarrolloregionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IniciodesarrolloregionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
