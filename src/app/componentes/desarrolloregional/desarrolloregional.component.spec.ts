import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesarrolloregionalComponent } from './desarrolloregional.component';

describe('DesarrolloregionalComponent', () => {
  let component: DesarrolloregionalComponent;
  let fixture: ComponentFixture<DesarrolloregionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesarrolloregionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesarrolloregionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
