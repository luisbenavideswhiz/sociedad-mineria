import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeescuchamosComponent } from './teescuchamos.component';

describe('TeescuchamosComponent', () => {
  let component: TeescuchamosComponent;
  let fixture: ComponentFixture<TeescuchamosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeescuchamosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeescuchamosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
