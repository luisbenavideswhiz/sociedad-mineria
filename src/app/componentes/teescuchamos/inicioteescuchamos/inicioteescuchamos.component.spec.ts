import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioteescuchamosComponent } from './inicioteescuchamos.component';

describe('InicioteescuchamosComponent', () => {
  let component: InicioteescuchamosComponent;
  let fixture: ComponentFixture<InicioteescuchamosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InicioteescuchamosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioteescuchamosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
