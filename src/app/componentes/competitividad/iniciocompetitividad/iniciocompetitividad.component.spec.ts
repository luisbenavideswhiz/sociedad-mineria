import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IniciocompetitividadComponent } from './iniciocompetitividad.component';

describe('IniciocompetitividadComponent', () => {
  let component: IniciocompetitividadComponent;
  let fixture: ComponentFixture<IniciocompetitividadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IniciocompetitividadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IniciocompetitividadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
