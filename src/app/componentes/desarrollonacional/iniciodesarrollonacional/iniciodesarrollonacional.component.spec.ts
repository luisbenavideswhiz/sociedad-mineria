import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IniciodesarrollonacionalComponent } from './iniciodesarrollonacional.component';

describe('IniciodesarrollonacionalComponent', () => {
  let component: IniciodesarrollonacionalComponent;
  let fixture: ComponentFixture<IniciodesarrollonacionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IniciodesarrollonacionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IniciodesarrollonacionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
