import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesarrollonacionalComponent } from './desarrollonacional.component';

describe('DesarrollonacionalComponent', () => {
  let component: DesarrollonacionalComponent;
  let fixture: ComponentFixture<DesarrollonacionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesarrollonacionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesarrollonacionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
