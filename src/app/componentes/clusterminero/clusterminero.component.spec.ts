import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClustermineroComponent } from './clusterminero.component';

describe('ClustermineroComponent', () => {
  let component: ClustermineroComponent;
  let fixture: ComponentFixture<ClustermineroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClustermineroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClustermineroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
