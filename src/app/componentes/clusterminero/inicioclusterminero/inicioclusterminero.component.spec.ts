import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioclustermineroComponent } from './inicioclusterminero.component';

describe('InicioclustermineroComponent', () => {
  let component: InicioclustermineroComponent;
  let fixture: ComponentFixture<InicioclustermineroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InicioclustermineroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioclustermineroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
