import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioindiceComponent } from './inicioindice.component';

describe('InicioindiceComponent', () => {
  let component: InicioindiceComponent;
  let fixture: ComponentFixture<InicioindiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InicioindiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioindiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
