import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Route } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EncabezadoComponent } from './componentes/encabezado/encabezado.component';
import { CuerpoComponent } from './componentes/cuerpo/cuerpo.component';
import { ForoComponent } from './componentes/foro/foro.component';
import { HistoriayactualidadComponent } from './componentes/historiayactualidad/historiayactualidad.component';
import { CienciaytecnologiaComponent } from './componentes/cienciaytecnologia/cienciaytecnologia.component';
import { BarraderechaComponent } from './componentes/barraderecha/barraderecha.component';
import { TambograndeComponent } from './componentes/historiayactualidad/tambogrande/tambogrande.component';
import { TiamariaComponent } from './componentes/historiayactualidad/tiamaria/tiamaria.component';
import { CongaComponent } from './componentes/historiayactualidad/conga/conga.component';
import { RioblancoComponent } from './componentes/historiayactualidad/rioblanco/rioblanco.component';
import { BuenaspracticasComponent } from './componentes/buenaspracticas/buenaspracticas.component';
import { AvancedeproyectosComponent } from './componentes/avancedeproyectos/avancedeproyectos.component';
import { DesarrollonacionalComponent } from './componentes/desarrollonacional/desarrollonacional.component';
import { DesarrolloregionalComponent } from './componentes/desarrolloregional/desarrolloregional.component';
import { CompetitividadComponent } from './componentes/competitividad/competitividad.component';
import { ClustermineroComponent } from './componentes/clusterminero/clusterminero.component';
import { FormalizacionmineraComponent } from './componentes/formalizacionminera/formalizacionminera.component';
import { SostenibilidadambientalComponent } from './componentes/sostenibilidadambiental/sostenibilidadambiental.component';
import { IndiceComponent } from './componentes/indice/indice.component';
import { TeescuchamosComponent } from './componentes/teescuchamos/teescuchamos.component';
import { Ejemplo1Component } from './componentes/cienciaytecnologia/ejemplo1/ejemplo1.component';
import { DigitalizacionmineraComponent } from './componentes/cienciaytecnologia/digitalizacionminera/digitalizacionminera.component';
import { EfectoeconomiaComponent } from './componentes/buenaspracticas/efectoeconomia/efectoeconomia.component';
import { ProyectosenexploracionComponent } from './componentes/avancedeproyectos/proyectosenexploracion/proyectosenexploracion.component';
import { PetitoriosminerosComponent } from './componentes/avancedeproyectos/petitoriosmineros/petitoriosmineros.component';
import { ConactividadpreliminarComponent } from './componentes/avancedeproyectos/conactividadpreliminar/conactividadpreliminar.component';
import { ProyectosenconstruccionComponent } from './componentes/avancedeproyectos/proyectosenconstruccion/proyectosenconstruccion.component';
import { ProyectosenoperacionComponent } from './componentes/avancedeproyectos/proyectosenoperacion/proyectosenoperacion.component';
import { combineLatest } from 'rxjs';
import { IniciodesarrollonacionalComponent } from './componentes/desarrollonacional/iniciodesarrollonacional/iniciodesarrollonacional.component';
import { IniciodesarrolloregionalComponent } from './componentes/desarrolloregional/iniciodesarrolloregional/iniciodesarrolloregional.component';
import { IniciocompetitividadComponent } from './componentes/competitividad/iniciocompetitividad/iniciocompetitividad.component';
import { InicioclustermineroComponent } from './componentes/clusterminero/inicioclusterminero/inicioclusterminero.component';
import { InicioformalizacionmineraComponent } from './componentes/formalizacionminera/inicioformalizacionminera/inicioformalizacionminera.component';
import { IniciosostenibilidadambientalComponent } from './componentes/sostenibilidadambiental/iniciosostenibilidadambiental/iniciosostenibilidadambiental.component';
import { InicioindiceComponent } from './componentes/indice/inicioindice/inicioindice.component';
import { InicioteescuchamosComponent } from './componentes/teescuchamos/inicioteescuchamos/inicioteescuchamos.component';
import { MineriainformalComponent } from './componentes/avancedeproyectos/mineriainformal/mineriainformal.component';
import { LineamientosComponent } from './componentes/lineamientos/lineamientos.component';
import { IndicecontenidosComponent } from './componentes/indicecontenidos/indicecontenidos.component';
import { DdhhComponent } from './componentes/ddhh/ddhh.component';
import { InicioddhhComponent } from './componentes/ddhh/inicioddhh/inicioddhh.component';
import { ForosanterioresComponent } from './componentes/forosanteriores/forosanteriores.component';
import { InicioforosanterioresComponent } from './componentes/forosanteriores/inicioforosanteriores/inicioforosanteriores.component';
import { LoginComponent } from './componentes/login/login.component';

// Login
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { environment } from 'src/environments/environment';

const routes: Route[] = [
  {path: 'app-foro', component: ForoComponent},

  {path: 'app-historiayactualidad', component: HistoriayactualidadComponent,
          children: [
                      {path: 'app-tambogrande', component: TambograndeComponent},
                      {path: 'app-tiamaria', component: TiamariaComponent},
                      {path: 'app-conga', component: CongaComponent},
                      {path: 'app-rioblanco', component: RioblancoComponent},

                      {path: '', redirectTo: '/app-historiayactualidad/app-tambogrande', pathMatch: 'full' }
                    ]
  },

  {path: 'app-cienciaytecnologia', component: CienciaytecnologiaComponent,
          children: [
            {path: 'app-ejemplo1', component: Ejemplo1Component},
            {path: 'app-digitalizacionminera', component: DigitalizacionmineraComponent},

            {path: '', redirectTo: '/app-cienciaytecnologia/app-ejemplo1', pathMatch: 'full' }
          ]
  },
        {path: 'app-buenaspracticas' , component: BuenaspracticasComponent,
        children: [
          {path: 'app-efectoeconomia', component: EfectoeconomiaComponent},

          {path: '', redirectTo: '/app-buenaspracticas/app-efectoeconomia', pathMatch: 'full'}
        ]
  },

        {path: 'app-avancedeproyectos', component: AvancedeproyectosComponent,
          children: [
            {path: 'app-petitoriosmineros', component: PetitoriosminerosComponent},
            {path: 'app-conactividadpreliminar', component: ConactividadpreliminarComponent},
            {path: 'app-proyectosenexploracion', component: ProyectosenexploracionComponent},
            {path: 'app-proyectosenconstruccion', component: ProyectosenconstruccionComponent},
            {path: 'app-proyectosenoperacion', component: ProyectosenoperacionComponent},
            {path: 'app-mineriainformal', component: MineriainformalComponent},

            {path: '', redirectTo: '/app-avancedeproyectos/app-petitoriosmineros', pathMatch: 'full'}
          ]
},

        {path: 'app-desarrollonacional', component: DesarrollonacionalComponent,
          children: [
            {path: 'app-iniciodesarrollonacional', component: IniciodesarrollonacionalComponent},

            {path: '', redirectTo: '/app-desarrollonacional/app-iniciodesarrollonacional', pathMatch: 'full'}
          ]
},

        {path: 'app-desarrolloregional', component: DesarrolloregionalComponent,
          children: [
            {path: 'app-iniciodesarrolloregional', component: IniciodesarrolloregionalComponent},

            {path: '', redirectTo: '/app-desarrolloregional/app-iniciodesarrolloregional', pathMatch: 'full'}
          ]
},

        {path: 'app-competitividad', component: CompetitividadComponent,
          children: [
            {path: 'app-iniciocompetitividad', component: IniciocompetitividadComponent},

            {path: '', redirectTo: '/app-competitividad/app-iniciocompetitividad', pathMatch: 'full'}
          ]
},

        {path: 'app-clusterminero', component: ClustermineroComponent,
          children: [
            {path: 'app-inicioclusterminero', component: InicioclustermineroComponent},

            {path: '', redirectTo: '/app-clusterminero/app-inicioclusterminero', pathMatch: 'full'}
          ]
},

        {path: 'app-formalizacionminera', component: FormalizacionmineraComponent,
          children: [
            {path: 'app-inicioformalizacionminera', component: InicioformalizacionmineraComponent},

            {path: '', redirectTo: '/app-formalizacionminera/app-inicioformalizacionminera', pathMatch: 'full'}
          ]
},

        {path: 'app-sostenibilidadambiental', component: SostenibilidadambientalComponent,
          children: [
            {path: 'app-iniciosostenibilidadambiental', component: IniciosostenibilidadambientalComponent},

            {path: '', redirectTo: '/app-sostenibilidadambiental/app-iniciosostenibilidadambiental', pathMatch: 'full'}
          ]
},

        {path: 'app-indice', component: IndiceComponent,
          children: [
            {path: 'app-inicioindice', component: InicioindiceComponent},

            {path: '', redirectTo: '/app-indice/app-inicioindice', pathMatch: 'full'}
          ]
},

        {path: 'app-teescuchamos', component: TeescuchamosComponent,
          children: [
            {path: 'app-inicioteescuchamos', component: InicioteescuchamosComponent},

            {path: '', redirectTo: '/app-teescuchamos/app-inicioteescuchamos', pathMatch: 'full'}
          ]

},

        {path: 'app-ddhh', component: DdhhComponent,
        children: [
          {path: 'app-inicioddhh', component: InicioddhhComponent},

          {path: '', redirectTo: '/app-ddhh/app-inicioddhh', pathMatch: 'full'}
        ]
},

        {path: 'app-forosanteriores', component: ForosanterioresComponent,
        children: [
          {path: 'app-inicioforosanteriores', component: InicioforosanterioresComponent},

          {path: '', redirectTo: '/app-forosanteriores/app-inicioforosanteriores', pathMatch: 'full'}
        ]
      },




  {path: 'app-buenaspracticas', component: BuenaspracticasComponent},
  {path: 'app-avancedeproyectos', component: AvancedeproyectosComponent},
  {path: 'app-desarrollonacional', component: DesarrollonacionalComponent},
  {path: 'app-desarrolloregional', component: DesarrolloregionalComponent},
  {path: 'app-competitividad', component: CompetitividadComponent},
  {path: 'app-clusterminero', component: ClustermineroComponent},
  {path: 'app-formalizacionminera', component: FormalizacionmineraComponent},
  {path: 'app-sostenibilidadambiental', component: SostenibilidadambientalComponent},
  {path: 'app-indice', component: IndiceComponent},
  {path: 'app-teescuchamos', component: TeescuchamosComponent},
  {path: 'app-lineamientos', component: LineamientosComponent},
  {path: 'app-indicecontenidos', component: IndicecontenidosComponent},
  {path: 'app-ddhh', component: DdhhComponent},
  {path: 'app-forosanteriores', component: ForosanterioresComponent},
  //{path: 'app-login', component: LoginComponent},

  {path: '', redirectTo: '/app-foro', pathMatch: 'full' }

];

@NgModule({
  declarations: [
    AppComponent,
    EncabezadoComponent,
    CuerpoComponent,
    ForoComponent,
    HistoriayactualidadComponent,
    CienciaytecnologiaComponent,
    BarraderechaComponent,
    TambograndeComponent,
    TiamariaComponent,
    CongaComponent,
    RioblancoComponent,
    BuenaspracticasComponent,
    AvancedeproyectosComponent,
    DesarrollonacionalComponent,
    DesarrolloregionalComponent,
    CompetitividadComponent,
    ClustermineroComponent,
    FormalizacionmineraComponent,
    SostenibilidadambientalComponent,
    IndiceComponent,
    TeescuchamosComponent,
    Ejemplo1Component,
    DigitalizacionmineraComponent,
    EfectoeconomiaComponent,
    ProyectosenexploracionComponent,
    PetitoriosminerosComponent,
    ConactividadpreliminarComponent,
    ProyectosenconstruccionComponent,
    ProyectosenoperacionComponent,
    IniciodesarrollonacionalComponent,
    IniciocompetitividadComponent,
    InicioclustermineroComponent,
    InicioformalizacionmineraComponent,
    IniciosostenibilidadambientalComponent,
    InicioindiceComponent,
    InicioteescuchamosComponent,
    MineriainformalComponent,
    LineamientosComponent,
    IndicecontenidosComponent,
    DdhhComponent,
    InicioddhhComponent,
    ForosanterioresComponent,
    InicioforosanterioresComponent,
    //LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

   constructor() {
  }


 }
