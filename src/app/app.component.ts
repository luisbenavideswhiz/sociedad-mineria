import { Component } from '@angular/core';
//import { LoginComponent } from './componentes/login/login.component';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from './auth/services/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AuthService]
})
export class AppComponent {
  title = 'forominangular';

  ocultar: boolean;
  mostrar: boolean;

  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  })

  constructor(private authSvc: AuthService) {
    this.ocultar = true;
    this.mostrar = false;
  }

  showlogin() {
    this.ocultar = !this.ocultar;
    this.mostrar = !this.mostrar;
  }

  onLogin() {
    const {email, password} = this.loginForm.value;
    this.authSvc.login(email, password)
    .then(r => {
      console.log('Login successfully', r);
      alert("Logueado con exito!");
      this.ocultar = false;
      this.mostrar = true;
    })
    .catch(err => {
      console.log('Login failed', err);
      alert("Login fallido! " + err.message);
      this.ocultar = true;
      this.mostrar = false;
    })

  }
}
