// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyC4TFjG6cy_ctdlVNLsBGPslTARQkqOn0s",
    authDomain: "demoforomind.firebaseapp.com",
    databaseURL: "https://demoforomind.firebaseio.com",
    projectId: "demoforomind",
    storageBucket: "demoforomind.appspot.com",
    messagingSenderId: "675135049339",
    appId: "1:675135049339:web:958cec2d3c2df36e06dcde"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
